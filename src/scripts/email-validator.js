// email-validator.js

// Module scoped variable for valid email endings
const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

// Function to validate the email address
function validate(email) {
  // Extract the domain part of the email address
  const emailDomain = email.substring(email.lastIndexOf("@") + 1);
  // Check if the domain part is in the list of valid email endings
  return VALID_EMAIL_ENDINGS.includes(emailDomain);
}

// Export the validate function to make it available for import
export { validate };
