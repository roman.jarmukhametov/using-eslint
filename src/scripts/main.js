// main.js
//Import styles
import "../styles/style.css";

// Import the JoinUsSection module
import { JoinUsSection } from "./join-us-section.js";

// Initialize the JoinUsSection module when the DOM is ready
document.addEventListener("DOMContentLoaded", function () {
  JoinUsSection.init();
});
