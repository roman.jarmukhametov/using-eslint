import { validate } from "./email-validator.js"; // Adjust the path as necessary

// Define a self-invoking function to encapsulate the JoinUsSection module. This pattern helps in creating private and public parts of the module.
const JoinUsSection = (function () {
  // Private function to create and return a section element with specific content and styles.
  function createSection() {
    // Create a <section> element and assign it a class for styling.
    const section = document.createElement("section");
    section.className = "app-section app-section--image-join";

    // Create a <h2> element for the section title, set its class, and text content.
    const h2 = document.createElement("h2");
    h2.className = "app-title";
    h2.textContent = "Join Our Program";

    // Create a <p> element for the section description, set its class, and text content.
    const p = document.createElement("p");
    p.className = "app-section__paragraph";
    p.textContent =
      "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

    // Create a <form> element for email subscription, set its class, and action attribute.
    const form = document.createElement("form");
    form.className = "app-section__form";
    form.action = "#";

    // Create an <input> element for email input, set its type, placeholder, and class.
    const emailInput = document.createElement("input");
    emailInput.type = "email";
    emailInput.placeholder = "Email";
    emailInput.className = "app-section__input--email";

    // Create another <input> element for the submit button, set its class, type, and value (button text).
    const submitButton = document.createElement("input");
    submitButton.className =
      "app-section__button app-section__button--subscribe";
    submitButton.type = "submit";
    submitButton.value = "Subscribe";

    // Append the email input and submit button to the form element.
    form.appendChild(emailInput);
    form.appendChild(submitButton);

    // Append the title (h2), description (p), and form to the section element.
    section.appendChild(h2);
    section.appendChild(p);
    section.appendChild(form);

    // Attach an event listener to the form to handle the submit event.
    form.addEventListener("submit", function (event) {
      event.preventDefault(); // Prevent the form from submitting in the traditional way.

      // Use the validate function to check the email input
      const isValidEmail = validate(emailInput.value);

      // Alert the user with the result
      if (isValidEmail) {
        alert("Thank you for subscribing!");
      } else {
        alert("Please enter a valid email address.");
      }

      console.log("Email entered:", emailInput.value); // Log the entered email to the console.
    });

    return section; // Return the fully constructed section element.
  }

  // Private function to insert the created section before the footer element on the page.
  function insertBeforeFooter() {
    const section = createSection(); // Create the section element.
    const footer = document.querySelector(".app-footer"); // Find the footer element in the DOM.
    footer.parentNode.insertBefore(section, footer); // Insert the section before the footer in the DOM.
  }

  // Public API of the JoinUsSection module.
  // Exposes the insertBeforeFooter method through a named property 'init'.
  return {
    init: insertBeforeFooter,
  };
})();

// Export the JoinUsSection module to make it available for import in other parts of the application.
export { JoinUsSection };
